package com.binary_studio.fleet_commander.core.subsystems;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

import static com.binary_studio.fleet_commander.core.common.Validator.isNameCorrect;

public final class DefenciveSubsystemImpl implements DefenciveSubsystem {

	private final String name;

	private final PositiveInteger impactReduction;

	private final PositiveInteger shieldRegen;

	private final PositiveInteger hullRegen;

	private final PositiveInteger capacitorUsage;

	private final PositiveInteger pgRequirement;

	public static DefenciveSubsystemImpl construct(String name, PositiveInteger powergridConsumption,
			PositiveInteger capacitorConsumption, PositiveInteger impactReductionPercent,
			PositiveInteger shieldRegeneration, PositiveInteger hullRegeneration) throws IllegalArgumentException {
		isNameCorrect(name);
		return new DefenciveSubsystemImpl(name, impactReductionPercent, shieldRegeneration, hullRegeneration,
				capacitorConsumption, powergridConsumption);
	}

	public DefenciveSubsystemImpl(String name, PositiveInteger impactReduction, PositiveInteger shieldRegen,
			PositiveInteger hullRegen, PositiveInteger capacitorUsage, PositiveInteger pgRequirement) {
		this.name = name;
		this.impactReduction = impactReduction;
		this.shieldRegen = shieldRegen;
		this.hullRegen = hullRegen;
		this.capacitorUsage = capacitorUsage;
		this.pgRequirement = pgRequirement;
	}

	@Override
	public PositiveInteger getPowerGridConsumption() {
		return this.pgRequirement;
	}

	@Override
	public PositiveInteger getCapacitorConsumption() {
		return this.capacitorUsage;
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public AttackAction reduceDamage(AttackAction incomingDamage) {
		double percentageOfDamageReduction = this.impactReduction.value() / 100.0;
		double minDamagePercentage = 0.05;
		Integer damage = incomingDamage.damage.value();
		if (this.impactReduction.value() > 95) {
			damage = (int) Math.ceil(damage * minDamagePercentage);
		}
		else {
			damage -= (int) Math.floor(damage * percentageOfDamageReduction);
		}
		return new AttackAction(PositiveInteger.of(damage), incomingDamage.attacker, incomingDamage.target,
				incomingDamage.weapon);
	}

	@Override
	public RegenerateAction regenerate() {
		return new RegenerateAction(this.shieldRegen, this.hullRegen);
	}

}
