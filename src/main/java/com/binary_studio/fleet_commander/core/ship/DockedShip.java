package com.binary_studio.fleet_commander.core.ship;

import java.util.Objects;

import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.exceptions.InsufficientPowergridException;
import com.binary_studio.fleet_commander.core.exceptions.NotAllSubsystemsFitted;
import com.binary_studio.fleet_commander.core.ship.contract.ModularVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class DockedShip implements ModularVessel {

	private final String name;

	private final PositiveInteger shieldHP;

	private final PositiveInteger hullHP;

	private PositiveInteger capacitor;

	private final PositiveInteger capacitorRegeneration;

	private PositiveInteger pg;

	private final PositiveInteger speed;

	private final PositiveInteger size;

	private AttackSubsystem attackSubsystem;

	private DefenciveSubsystem defenciveSubsystem;

	public static DockedShip construct(String name, PositiveInteger shieldHP, PositiveInteger hullHP,
			PositiveInteger powergridOutput, PositiveInteger capacitorAmount, PositiveInteger capacitorRechargeRate,
			PositiveInteger speed, PositiveInteger size) {

		return new DockedShip(name, shieldHP, hullHP, capacitorAmount, capacitorRechargeRate, powergridOutput, speed,
				size);
	}

	public DockedShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger capacitor,
			PositiveInteger capacitorRegeneration, PositiveInteger pg, PositiveInteger speed, PositiveInteger size) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.capacitor = capacitor;
		this.capacitorRegeneration = capacitorRegeneration;
		this.pg = pg;
		this.speed = speed;
		this.size = size;
	}

	public String getName() {
		return this.name;
	}

	public PositiveInteger getSpeed() {
		return this.speed;
	}

	@Override
	public void fitAttackSubsystem(AttackSubsystem subsystem) throws InsufficientPowergridException {
		if (Objects.nonNull(subsystem)) {
			fitSubSystem(subsystem.getPowerGridConsumption().value());
		}
		this.attackSubsystem = subsystem;
	}

	private void fitSubSystem(Integer pgConsumption) throws InsufficientPowergridException {
		int missing = this.pg.value() - pgConsumption;
		if (missing < 0) {
			missing *= -1;
			throw new InsufficientPowergridException(missing);
		}
		this.pg = PositiveInteger.of(missing);
	}

	@Override
	public void fitDefensiveSubsystem(DefenciveSubsystem subsystem) throws InsufficientPowergridException {
		if (Objects.nonNull(subsystem)) {
			fitSubSystem(subsystem.getPowerGridConsumption().value());
		}
		this.defenciveSubsystem = subsystem;
	}

	public CombatReadyShip undock() throws NotAllSubsystemsFitted {
		if (Objects.isNull(this.attackSubsystem) && Objects.nonNull(this.defenciveSubsystem)) {
			throw NotAllSubsystemsFitted.attackMissing();
		}
		else if (Objects.isNull(this.defenciveSubsystem) && Objects.nonNull(this.attackSubsystem)) {
			throw NotAllSubsystemsFitted.defenciveMissing();
		}
		else if (Objects.isNull(this.attackSubsystem)) {
			throw NotAllSubsystemsFitted.bothMissing();
		}
		return new CombatReadyShip(this.name, this.shieldHP, this.hullHP, this.capacitor, this.capacitorRegeneration,
				this.pg, this.speed, this.size, this.attackSubsystem, this.defenciveSubsystem);
	}

}
