package com.binary_studio.fleet_commander.core.ship;

import java.util.Optional;
import java.util.logging.Logger;

import com.binary_studio.fleet_commander.core.actions.attack.AttackAction;
import com.binary_studio.fleet_commander.core.actions.defence.AttackResult;
import com.binary_studio.fleet_commander.core.actions.defence.RegenerateAction;
import com.binary_studio.fleet_commander.core.common.Attackable;
import com.binary_studio.fleet_commander.core.common.PositiveInteger;
import com.binary_studio.fleet_commander.core.ship.contract.CombatReadyVessel;
import com.binary_studio.fleet_commander.core.subsystems.contract.AttackSubsystem;
import com.binary_studio.fleet_commander.core.subsystems.contract.DefenciveSubsystem;

public final class CombatReadyShip implements CombatReadyVessel {

	private static final Logger log = Logger.getLogger("CombatReadyShip");

	private final String name;

	private PositiveInteger shieldHP;

	private PositiveInteger hullHP;

	private PositiveInteger capacitor;

	private final PositiveInteger capacitorRegeneration;

	private PositiveInteger pg;

	private final PositiveInteger speed;

	private final PositiveInteger size;

	private final AttackSubsystem attackSubsystem;

	private final DefenciveSubsystem defenciveSubsystem;

	private final Integer capacitorBoundary;

	private final Integer shieldHPBoundary;

	private final Integer hullHPBoundary;

	public CombatReadyShip(String name, PositiveInteger shieldHP, PositiveInteger hullHP, PositiveInteger capacitor,
			PositiveInteger capacitorRegeneration, PositiveInteger pg, PositiveInteger speed, PositiveInteger size,
			AttackSubsystem attackSubsystem, DefenciveSubsystem defenciveSubsystem) {
		this.name = name;
		this.shieldHP = shieldHP;
		this.hullHP = hullHP;
		this.capacitor = capacitor;
		this.capacitorRegeneration = capacitorRegeneration;
		this.pg = pg;
		this.speed = speed;
		this.size = size;
		this.attackSubsystem = attackSubsystem;
		this.defenciveSubsystem = defenciveSubsystem;
		this.capacitorBoundary = capacitor.value();
		this.shieldHPBoundary = shieldHP.value();
		this.hullHPBoundary = hullHP.value();
	}

	@Override
	public void endTurn() {
		log.info("END");
		Integer regenCapacity = this.capacitor.value() + this.capacitorRegeneration.value();
		if (regenCapacity <= this.capacitorBoundary) {
			this.capacitor = PositiveInteger.of(regenCapacity);
		}
		else {
			this.capacitor = PositiveInteger.of(this.capacitorBoundary);
		}
	}

	@Override
	public void startTurn() {
		log.info("START");
	}

	@Override
	public String getName() {
		return this.name;
	}

	@Override
	public PositiveInteger getSize() {
		return this.size;
	}

	@Override
	public PositiveInteger getCurrentSpeed() {
		return this.speed;
	}

	@Override
	public Optional<AttackAction> attack(Attackable target) {
		Optional<AttackAction> attackAction = Optional.empty();
		if (this.capacitor.value() >= this.attackSubsystem.getCapacitorConsumption().value()) {
			PositiveInteger damage = this.attackSubsystem.attack(target);
			attackAction = Optional.of(new AttackAction(damage, this, target, this.attackSubsystem));
			this.capacitor = PositiveInteger
					.of(this.capacitor.value() - this.attackSubsystem.getCapacitorConsumption().value());
		}
		return attackAction;
	}

	@Override
	public AttackResult applyAttack(AttackAction attack) {
		AttackAction action = this.defenciveSubsystem.reduceDamage(attack);
		boolean isDestroyed = applyDamage(action.damage.value());
		AttackResult result;
		if (isDestroyed) {
			result = new AttackResult.Destroyed();
		}
		else {
			result = new AttackResult.DamageRecived(action.weapon, action.damage, action.target);
		}
		return result;
	}

	private boolean applyDamage(Integer damage) {
		Boolean dead = Boolean.FALSE;
		if (this.shieldHP.value() > 0) {
			this.shieldHP = reduceProperty(damage, this.shieldHP);
		}
		if (this.shieldHP.value().equals(0) && this.hullHP.value() > 0) {
			this.hullHP = reduceProperty(damage, this.hullHP);
		}
		else if (this.shieldHP.value().equals(0) && this.hullHP.value().equals(0)) {
			dead = Boolean.TRUE;
		}
		return dead;
	}

	private PositiveInteger reduceProperty(Integer damage, PositiveInteger hp) {
		int shield = hp.value() - damage;
		if (shield < 0) {
			hp = PositiveInteger.of(0);
		}
		else {
			hp = PositiveInteger.of(shield);
		}
		return hp;
	}

	@Override
	public Optional<RegenerateAction> regenerate() {
		Optional<RegenerateAction> regenerateAction = Optional.empty();
		if (this.defenciveSubsystem.getCapacitorConsumption().value() <= this.capacitor.value()) {
			RegenerateAction tempAction = this.defenciveSubsystem.regenerate();
			Integer shieldRegenCapacity = tempAction.shieldHPRegenerated.value();
			Integer hullRegenCapacity = tempAction.hullHPRegenerated.value();
			regenerateAction = Optional
					.of(new RegenerateAction(regen(shieldRegenCapacity, this.shieldHP, this.shieldHPBoundary),
							regen(hullRegenCapacity, this.hullHP, this.hullHPBoundary)));
			this.capacitor = PositiveInteger
					.of(this.capacitor.value() - this.defenciveSubsystem.getCapacitorConsumption().value());
		}
		return regenerateAction;
	}

	private PositiveInteger regen(Integer regenCapacity, PositiveInteger hp, Integer boundary) {
		Integer regenerated = boundary - hp.value();
		if (regenerated <= regenCapacity) {
			hp = PositiveInteger.of(hp.value() + regenerated);
		}
		else {
			hp = PositiveInteger.of(hp.value() + regenCapacity);
			regenerated = regenCapacity;
		}
		return PositiveInteger.of(regenerated);
	}

}
