package com.binary_studio.fleet_commander.core.common;

import java.util.Objects;

public final class Validator {

	private Validator() {
	}

	public static void isNameCorrect(String name) {
		if (Objects.isNull(name) || name.trim().isEmpty()) {
			throw new IllegalArgumentException("Name should be not null and not empty");
		}
	}

}
