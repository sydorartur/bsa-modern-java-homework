package com.binary_studio.uniq_in_sorted_stream;

import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Stream;

public final class UniqueSortedStream {

	private UniqueSortedStream() {
	}

	public static <T> Stream<Row<T>> uniqueRowsSortedByPK(Stream<Row<T>> stream) {
		return stream.filter(uniqueByPK(Row::getPrimaryId));
	}

	public static <T> Predicate<T> uniqueByPK(Function<? super T, ?> getPK) {
		Map<Object, Boolean> uniqueElem = new HashMap<>();
		return filter -> uniqueElem.putIfAbsent(getPK.apply(filter), Boolean.TRUE) == null;
	}

}
