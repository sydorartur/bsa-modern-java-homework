package com.binary_studio.tree_max_depth;

import java.util.*;
import java.util.stream.Collectors;

public final class DepartmentMaxDepth {

	private DepartmentMaxDepth() {
	}

	public static Integer calculateMaxDepth(Department rootDepartment) {
		if (Objects.isNull(rootDepartment)) {
			return 0;
		}
		else {
			return findMaxDepth(rootDepartment);
		}
	}

	public static Integer findMaxDepth(Department root) {
		Integer depth = 1;
		int numberOfDepartments = 1;
		Queue<Department> departments = new LinkedList<>(Collections.singletonList(root));
		while (true) {
			List<Department> activeDepartments = findAllActiveSubDepartments(departments);
			departments.addAll(activeDepartments);
			numberOfDepartments = deleteParentDepartments(departments, numberOfDepartments);
			if (numberOfDepartments == 0) {
				break;
			}
			depth++;
		}
		return depth;
	}

	private static int deleteParentDepartments(Queue<Department> departments, int numberOdParent) {
		for (int i = 0; i < numberOdParent; i++) {
			departments.poll();
		}
		return departments.size();
	}

	private static List<Department> findAllActiveSubDepartments(Queue<Department> departments) {
		List<Department> allActive = new LinkedList<>();
		for (Department department : departments) {
			allActive.addAll(deleteInactiveDepartments(department.subDepartments));
		}
		return allActive;
	}

	private static List<Department> deleteInactiveDepartments(List<Department> departments) {
		return departments.stream().filter(Objects::nonNull).collect(Collectors.toList());
	}

}
